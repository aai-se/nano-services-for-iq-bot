import json,os,csv,re

### CSV Format Expected: ###

#"Sentence","IT","HR"
#"my VPN is broken","TRUE","FALSE"

###########################

### Output Expected: ###

#train_data = [
#    (u"That was very bad", {"cats": {"POSITIVE": 0, "NEGATIVE": 1}}),
#    (u"That was great!", {"cats": {"POSITIVE": 1, "NEGATIVE": 0}})
#]
###########################

def CsvToSpacyTrainData(inputcsvfile):
    train_data = []
    labels = []
    with open(inputcsvfile) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        FormattedListForEntity = []

        for row in csv_reader:
            if line_count == 0:
                labels = row[1:]
                #print(f'NER Custom Tags are {", ".join(NER_TAGS)}')
                line_count += 1

            else:
                Sentence = row[0]
                Values = row[1:]

                #FormattedRow = (Sentence,{"cats":})
                idxvalue = 0
                CATS = {}
                for v in Values:

                    if Values[idxvalue].lower() == "true":
                        CATS[labels[idxvalue-1]] = 0
                    else:
                        CATS[labels[idxvalue-1]] = 1
                    idxvalue+=1

                Element = (Sentence,{"cats":CATS})
                FormattedListForEntity.append(Element)


                #print("DEBUG, items in line: "+str(len(ListOfEntitiesTagged)))

                line_count += 1
        #print (FormattedListForEntity)
        #print(labels)
        return labels,FormattedListForEntity
