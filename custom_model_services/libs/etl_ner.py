import json,os,csv,re

### CSV Format Expected: ###
#Sentence,ORG,AMT,CUR,DATE
#"Automation Anywhere has won a prize of 1B Dollars USD on Jan 1st, 2019",Automation Anywhere ,1B Dollars,USD,"Jan 1st, 2019"
#"Sony raised 10,000 USD for something on March 2nd",Sony,"10,000",USD,March 2nd
###########################

### Output Expected: ###
#TRAIN_DATA = [
#    ("Who is Shaka Khan?", {"entities": [(7, 17, "PERSON")]}),
#    ("I like London and Berlin.", {"entities": [(7, 13, "LOC"), (18, 24, "LOC")]}),
#]
###########################

def CsvToSpacyTrainData(inputcsvfile):
    train_data = []
    NER_TAGS = []
    with open(inputcsvfile,'r', encoding="ISO-8859-1") as csv_file:
        #csv_reader = read_csv(csv_file, encoding = "ISO-8859-1")
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            print("Processing Line: "+str(line_count))
            if line_count == 0:
                NER_TAGS = row[1:]
                #print(f'NER Custom Tags are {", ".join(NER_TAGS)}')
                line_count += 1

            else:
                Sentence = row[0]
                Entities = row[1:]
                FormattedListForEntity = []
                ListOfTags = []
                entidx = 0
                for ent in Entities:
                    subentities = []
                    if ent:
                        if "|" in ent:
                            subentities = ent.split("|")
                        else:
                            subentities.append(ent)
                        for subent in subentities:
                            #print("DEBUG:"+subent)
                            match = re.search("("+subent+")", Sentence)
                            if(match):
                                StartChar = match.start(1)
                                EndChar = StartChar + len(subent)
                                #print("Debug:"+str(len(NER_TAGS)))
                                Tag = NER_TAGS[entidx]
                                #print("Tagged: ["+ent+"] at: ["+str(StartChar)+"-"+str(EndChar)+"] as ["+Tag+"]")
                                ListOfTags.append((StartChar,EndChar,Tag))

                        entidx += 1

                #print("DEBUG, items in line: "+str(len(ListOfEntitiesTagged)))

                line_count += 1
                #("I like London and Berlin.", {"entities": [(7, 13, "LOC"), (18, 24, "LOC")]})
                train_data.append((Sentence,{"entities":ListOfTags}))

            #print("DEBUG, total: "+str(len(train_data)))
        return NER_TAGS,train_data
