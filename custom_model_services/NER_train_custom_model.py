#!/usr/bin/env python
# coding: utf8
"""
Train a Custom NER Model
Compatible with: spaCy v2.0.0+
Last tested with: v2.1.0
"""
from __future__ import unicode_literals, print_function
import os,sys,plac,random,spacy
from pathlib import Path
from spacy.util import minibatch, compounding
sys.path.insert(0, './libs')
from etl_ner import CsvToSpacyTrainData
from datetime import datetime

"""
    Set up:
        - the Model Name,
        - the CSV file containing the data,
        - the Language,
        - the percentage of data to use for training vs testing
"""

MODEL_NAME = "NER2" # NER Model Name to create and train
TRAIN_DATA_FILE = './csvs/DHL_NER_Tagging copy.csv' # Spreadsheet in Sentence,ENTITY1,ENTITY2, etc. format
MODEL_LANGUAGE = 'en' # language to use to build the model
MODEL_STORAGE_FOLDER = "./models" # Folder in which to store the Custom NER Model produced
DATA_PERCENTAGE_FOR_TRAINING = 70 # Percentage of total data used for Training (the rest is used for testing)


# Loading data from CSV File and splitting into into Training and Testing sets
def load_data():
    # Convert source csv file into a set of labels (String[] and Training Data)
    NER_TAGS,ALL_DATA = CsvToSpacyTrainData(TRAIN_DATA_FILE)

    print(ALL_DATA)
    # Randomize the array containing all the labeled data
    random.shuffle(ALL_DATA)

    TotalLengthOfData = len(ALL_DATA)
    LengthOfTrainingData = round(TotalLengthOfData * DATA_PERCENTAGE_FOR_TRAINING / 100)
    LengthOfTestData = TotalLengthOfData - LengthOfTrainingData

    TRAIN_DATA = ALL_DATA[0:LengthOfTrainingData]

    TEST_DATA = ALL_DATA[-LengthOfTestData:]
    return TRAIN_DATA, TEST_DATA


def main(model=None, output_dir=None, n_iter=100):

    TRAIN_DATA,TEST_DATA = load_data()

    nlp = spacy.blank(MODEL_LANGUAGE)  # create blank Language class based on input language

    # Create a new Pipeline
    ner = nlp.create_pipe("ner")
    nlp.add_pipe(ner, last=True)

    # add labels - not an efficient way to do this..
    for _, annotations in TRAIN_DATA:
        for ent in annotations.get("entities"):
            # Entity: Start Pos, End Pos, Label
            ner.add_label(ent[2])

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "ner"]
    #print("Debug Other Pipes:"+str(other_pipes))
    with nlp.disable_pipes(*other_pipes):  # only train NER
        # New Model: reset and initialize the weights randomly
        nlp.begin_training()

        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(TRAIN_DATA, size=compounding(4.0, 32.0, 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(
                    texts,  # batch of texts
                    annotations,  # batch of annotations
                    drop=0.5,  # dropout - make it harder to memorise data
                    losses=losses,
                )
            print("Losses: ", losses)

# End of Model Training

    # test the trained model
    for text, _ in TEST_DATA:
        doc = nlp(text)
        print("Entities: ", [(ent.text, ent.label_) for ent in doc.ents])
        #print("Tokens", [(t.text, t.ent_type_, t.ent_iob) for t in doc])

    """ Export Model to Filesystem, giving each iteration a timestamp to save it"""
    output_dir =MODEL_STORAGE_FOLDER+"/"+MODEL_NAME+"-"+datetime.now().strftime('%Y%m%d%H%M%S')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    nlp.to_disk(output_dir)
    print("Saved model to: ", output_dir)



if __name__ == "__main__":
    plac.call(main)
