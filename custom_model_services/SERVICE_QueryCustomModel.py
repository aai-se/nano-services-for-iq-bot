#######################################
#
# Nano Service for Custom NER Models (Spacy)
#
# Prerequisites:
# Install Python 3.7+ (make sure to add python to your PATH, the installer has an option for it)
# Install Conda: https://www.anaconda.com/distribution/#download-section
# From a Conda Prompt:
#   Install Spacy ()
#   Install Spacy's Language models (list available here: https://spacy.io/usage/models) (ex: english: python -m spacy download en_core_web_sm)
#
#         !!!!!!! IMPORTANT NOTE !!!!!
#   THIS SERVICE NEEDS TO BE STARTED IN AN ANACONDA PROMPT!! It will fail if you start it in a normal CLI Prompt
#
########################################
from __future__ import unicode_literals, print_function
DESCRIPTION = "NER Custom Model Query Service"
VERSION = 0.1
#
#######################################
import os,json,plac,random,collections,spacy,flask
from flask import Flask,request,jsonify,flash,redirect,url_for,send_file
from pathlib import Path
from spacy.util import minibatch, compounding

app = flask.Flask(__name__)
app.secret_key = 'Oh what a big secret this is!!'

INPUT_PATH = "./models"

def getAllowedPaths():
    #remove the backupdate folder
    AllFolders = os.listdir(INPUT_PATH)
    AllFolders.remove(".gitkeep")
    return AllFolders

def queryCustomClassifierModel(textToAnalyze,ModelName):

    texts = []
    text = textToAnalyze
    texts.append(text)
    model_dir = INPUT_PATH+"/"+ModelName
    # test the saved model
    print("Loading from: ", model_dir)
    nlp2 = spacy.load(model_dir)

    doc = nlp2(text)

    print(doc.cats)

    return doc.cats


def queryCustomNERModel(textToAnalyze,ModelName):

    texts = []
    text = textToAnalyze
    texts.append(text)
    model_dir = INPUT_PATH+"/"+ModelName
    # test the saved model
    print("Loading from: ", model_dir)
    nlp2 = spacy.load(model_dir)

    doc = nlp2(text)
    print("Entities: ", [(ent.text, ent.label_) for ent in doc.ents])
    #print("Tokens", [(t.text, t.ent_type_, t.ent_iob) for t in doc])

    nlp = nlp2

    with nlp.disable_pipes('ner'):
        doc = nlp(text)

    #threshold = 0.2
    beams = nlp.entity.beam_parse([ doc ], beam_width = 16, beam_density = 0.0001)

    entity_scores = collections.defaultdict(float)
    for beam in beams:
        for score, ents in nlp.entity.moves.get_beam_parses(beam):
            for start, end, label in ents:
                entity_scores[(start, end, label)] += score

    EntitiesAndScores = []

    for key in entity_scores:

        start, end, label = key
        score = entity_scores[key]
        text = doc[start:end]
        myEnt = ReadableEntity(str(text),str(label),str(score),start,end)
        EntitiesAndScores.append(myEnt)

    return [ob.__dict__ for ob in EntitiesAndScores]

@app.route("/")
def hello():
    return '{"desc":'+DESCRIPTION+',"version":"'+VERSION+'"}'

@app.route('/models', methods = ['GET', 'POST'])
def GetModels ():
    #IF Get, list models
    if flask.request.method == 'GET':
        try:
            AllFolders = getAllowedPaths()
            RawJsonArray = json.dumps(AllFolders)
            JSON = {
            "models":AllFolders
            }
            return jsonify(JSON)
        except Exception as e:
            return "Error:" +str(e)


@app.route('/models/ner', methods = ['GET','POST'])
def InvokeNERModel ():
    if flask.request.method == 'POST':
        content = request.get_json()
        if 'model' not in content:
            return jsonify({"Status" : "Error","Message":"Missing model parameter"})

        if 'text' not in content:
            return jsonify({"Status" : "Error","Message":"Missing text parameter"})

        TEXT = content['text']
        MODEL = content['model']

        AllEntities = queryCustomNERModel(TEXT,MODEL)
        JSON = {
        "entities":AllEntities
        }
        return jsonify(JSON)


@app.route('/models/classifier', methods = ['GET','POST'])
def InvokeClassifierModel ():
    if flask.request.method == 'POST':
        content = request.get_json()
        if 'model' not in content:
            return jsonify({"Status" : "Error","Message":"Missing model parameter"})

        if 'text' not in content:
            return jsonify({"Status" : "Error","Message":"Missing text parameter"})

        TEXT = content['text']
        MODEL = content['model']

        AllEntities = queryCustomClassifierModel(TEXT,MODEL)
        JSON = {
        "categories":AllEntities
        }
        return jsonify(JSON)

class ReadableEntity:
    def __init__(self,text,label,score,start,end):
        self.text = text
        self.label = label
        self.score = score
        self.pos_beg = start
        self.pos_end = end
