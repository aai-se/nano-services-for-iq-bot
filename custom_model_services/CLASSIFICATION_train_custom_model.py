import spacy,sys,random,os,plac,random
sys.path.insert(0, './libs')
from etl_classification import CsvToSpacyTrainData
from spacy.util import minibatch, compounding
from datetime import datetime

MODEL_NAME = "CLASSIFIER1" # NER Model Name to create and train
TRAIN_DATA_FILE = './csvs/CLASSIFICATION_Tagging_Example1.csv' # Spreadsheet in Sentence,ENTITY1,ENTITY2, etc. format
MODEL_LANGUAGE = 'en' # language to use to build the model
MODEL_STORAGE_FOLDER = "./models" # Folder in which to store the Custom NER Model produced
TEST_SENTENCES = [
    u'Hi, my VPN is down',
    u"Hi, what's my PTO balance?"
]
def load_data():
    return CsvToSpacyTrainData(TRAIN_DATA_FILE)

def main(n_iter=20):

    labels,train_data = load_data()
    nlp = spacy.blank(MODEL_LANGUAGE)


    textcat = nlp.create_pipe('textcat')
    nlp.add_pipe(textcat, last=True)
    for label in labels:
        textcat.add_label(label)


    optimizer = nlp.begin_training()


    for itn in range(n_iter):
        print("Training Iteration: "+str(itn))

        for doc, gold in train_data:
            nlp.update([doc], [gold], sgd=optimizer)

    for s in TEST_SENTENCES:

        doc = nlp(s)
        print(s+" | " +str(doc.cats))

    """ Export Model to Filesystem, giving each iteration a timestamp to save it"""
    output_dir =MODEL_STORAGE_FOLDER+"/"+MODEL_NAME+"-"+datetime.now().strftime('%Y%m%d%H%M%S')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    nlp.to_disk(output_dir)
    print("Saved model to: ", output_dir)


main()
