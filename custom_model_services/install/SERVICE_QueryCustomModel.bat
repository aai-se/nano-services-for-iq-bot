REM Conda needs to be installed on Windows
REM Need to install Conda first (https://conda.io/en/latest/)
call C:\ProgramData\Anaconda3\Scripts\activate.bat
echo "WARNING: this script should be ran in an Anaconda CLI Window."
pip install Spacy
pip install Flask
pip install plac
python -m spacy download en_core_web_sm
python -m spacy download fr_core_news_sm
python -m spacy download es_core_news_sm
python -m spacy download pt_core_news_sm