#######################################
#
# Nano Service to expose IQ Bot output files to download
#
# Prerequisites:
# Install Python 3.7+ (make sure to add python to your PATH, the installer has an option for it)
# Install Conda: https://www.anaconda.com/distribution/#download-section
# From a Conda Prompt:
#   Install Spacy ()
#   Install Spacy's Language models (list available here: https://spacy.io/usage/models) (ex: english: python -m spacy download en_core_web_sm)
#   Install textblob (pip install textblob)
#
#         !!!!!!! IMPORTANT NOTE !!!!!
#   THIS SERVICE NEEDS TO BE STARTED IN AN ANACONDA PROMPT!! It will fail if you start it in a normal CLI Prompt
#
########################################
DESCRIPTION = "Language Skills Service"
VERSION = 0.1
#
#######################################
import os,json
import spacy

from flask import Flask,request,jsonify,flash,redirect,url_for,send_file
from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer

AVAILABLE_LANGUAGES ={
		'en_lg': 'en_core_web_lg',
        'en_sm': 'en_core_web_sm',
		'en_md': 'en_core_web_md',
        'de': 'de_core_news_sm',
        'fr': 'fr_core_news_sm',
        'es': 'es_core_news_sm',
        'pt': 'pt_core_news_sm',
        'it': 'it_core_news_sm',
        'nl': 'nl_core_news_sm'
    }

Helper = {
    "_desc":DESCRIPTION,
    "_version":VERSION,
    "Mandatory Input Parameters" : "",
    "Optional Input Parameters" : ""
}

app = Flask(__name__)
app.secret_key = 'Oh what a big secret this is!!'

@app.route("/")
def hello():
    return jsonify(Helper)

# Done, returns the list of folders under the IQ Bot output folder (List of Learning Instances)
@app.route('/language', methods = ['GET', 'POST'])
def GetLanguage ():
    content = request.get_json()
    TextToAnalyze = content['text']
    b = TextBlob(TextToAnalyze)
    LANG = b.detect_language()
    #print("DEBUG:"+LANG) #p_neg or #classification
    return jsonify({"language" : LANG})
    #return jsonify({"sentiment" : opinionStd.sentiment.classification,"p_pos":opinionStd.sentiment.p_pos,"p_neg":opinionStd.sentiment.p_neg})

@app.route('/NER', methods = ['GET', 'POST'])
def GetEntities ():
    content = request.get_json()
    LANG = content['language']
    TEXT = content['text']
    NLPModelToUse = AVAILABLE_LANGUAGES[LANG]
    if NLPModelToUse == "":
        return jsonify({"Status" : "Error","message":"Language not supported:"+LANG})

    nlp = spacy.load(NLPModelToUse)
    doc = nlp(TEXT)
    AllEntities = []
    for ent in doc.ents:

        NewEnt = NamedEntity(ent.text,ent.label_,ent.start_char,ent.end_char)
        AllEntities.append(NewEnt)

    myObj = [ob.__dict__ for ob in AllEntities]
    JSON = {
    "entities":myObj
    }
    return jsonify(JSON)
    #return [ob.__dict__ for ob in AllEntities]

@app.route('/sentiment', methods = ['GET', 'POST'])
def GetSentiment ():
    print("DEBUG Text"+str(request.get_json()))
    content = request.get_json()
    TextToAnalyze = content['text']
    opinionBayes = TextBlob(TextToAnalyze, analyzer=NaiveBayesAnalyzer())
    opinionStd = TextBlob(TextToAnalyze)
    print("DEBUG:"+str(opinionStd.sentiment)) #p_neg or #classification
    OutputStr = '{"polarity" : '+str(opinionStd.sentiment.polarity)+',"subjectivity":'+str(opinionStd.sentiment.subjectivity)+'}'
    print("RAW:"+OutputStr)
    return OutputStr
    #return jsonify({"polarity" : opinionStd.sentiment.polarity,"subjectivity":opinionStd.sentiment.subjectivity})
    #return jsonify({"sentiment" : opinionStd.sentiment.classification,"p_pos":opinionStd.sentiment.p_pos,"p_neg":opinionStd.sentiment.p_neg})

class NamedEntity:
    def __init__(self,text,label,start_char,end_char):
        self.text = text
        self.label = label
        self.start_char = start_char
        self.end_char = end_char
