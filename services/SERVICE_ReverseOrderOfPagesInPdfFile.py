#######################################
#
# Nano Service to convert Country Name to Country Code (ex: France to FR)
#
DESCRIPTION = "Reverses the order of Pages in PDF a Document"
VERSION = 0.1
#
#######################################

from PyPDF2 import PdfFileWriter, PdfFileReader
from flask import Flask,request,jsonify
import os

app = Flask(__name__)

@app.route("/")
def hello():
    return '{"desc":'+DESCRIPTION+',"version":"'+VERSION+'"}'

#Expected:
# {"filepath":"c:/test/my.pdf"}
@app.route("/reverse", methods=['POST'])
def reverseIt():
    #print (request.is_json)
    content = request.get_json()
    #print("DEBUG:"+content['country'])
    FilePath = content['filepath']
    #print(os.path.split(FilePath))
    OutputPath =  getOutputFilePath(FilePath)
    reverseAPdf(FilePath,OutputPath)

    #return jsonify({"path":OutputPath})
    return OutputPath

@app.route("/split", methods=['POST'])
def splitAll():
    content = request.get_json()
    #print("DEBUG:"+content['country'])
    FilePath = content['filepath']
    splitPdf(FilePath)
    return "done"


def splitPdf(myPath):
    inputpdf = PdfFileReader(open(myPath, "rb"))
    mySplit = os.path.split(myPath)
    PathOnly=mySplit[0]
    FileNameWithExt=mySplit[1]
    FileName=FileNameWithExt.split(".")[0]
    for i in range(inputpdf.numPages):
        output = PdfFileWriter()
        output.addPage(inputpdf.getPage(i))
        OutputPath=PathOnly+"/"+FileName
        with open(OutputPath+"-page-%s.pdf" % i, "wb") as outputStream:
            output.write(outputStream)

def getOutputFilePath(myPath):
    mySplit = os.path.split(myPath)
    PathOnly=mySplit[0]
    FileNameWithExt=mySplit[1]
    FileName=FileNameWithExt.split(".")[0]
    Ext=FileNameWithExt.split(".")[1]
    OutputPath=PathOnly+'/'+"REV_"+FileNameWithExt
    return OutputPath

def reverseAPdf(InputFile, OutputFile):
    output_pdf = PdfFileWriter()

    with open(InputFile, 'rb') as readfile:
        input_pdf = PdfFileReader(readfile)
        total_pages = input_pdf.getNumPages()
        for page in range(total_pages - 1, -1, -1):
            output_pdf.addPage(input_pdf.getPage(page))
            with open(OutputFile, "wb") as writefile:
                output_pdf.write(writefile)
    return ""
