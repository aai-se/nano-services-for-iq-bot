#######################################
#
# Nano Service to parse raw text from tweets
#
# Prerequisites:
# Install Python 3.7+ (make sure to add python to your PATH, the installer has an option for it)
#
# IMPORTANT: This service is NOT ready for Prime Time..
#
########################################
DESCRIPTION = "Twitter Service"
VERSION = 0.1
#
#######################################
import os,json,re

from flask import Flask,request,jsonify,flash,redirect,url_for,send_file
from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer


app = Flask(__name__)
app.secret_key = 'Oh what a big secret this is!!'

def GetSentiment (mytext):

    #opinionBayes = TextBlob(mytext, analyzer=NaiveBayesAnalyzer())
    opinionStd = TextBlob(mytext)
    #print("DEBUG:"+str(opinionStd.sentiment)) #p_neg or #classification
    #return jsonify({"polarity" : opinionStd.sentiment.polarity,"subjectivity":opinionStd.sentiment.subjectivity})
    return opinionStd.sentiment.polarity,opinionStd.sentiment.subjectivity
    #return jsonify({"sentiment" : opinionStd.sentiment.classification,"p_pos":opinionStd.sentiment.p_pos,"p_neg":opinionStd.sentiment.p_neg})

# Done, returns the list of folders under the IQ Bot output folder (List of Learning Instances)
@app.route('/extract', methods = ['GET', 'POST'])
def GetInfo ():
    rawtext = request.data.decode("utf-8")

    print(type(rawtext))
    body = rawtext.replace("\t","")
    jbody = json.loads(body)
    
    #content = request.get_json()
    TextToAnalyze = jbody['text']


    try:
        m0 = re.search(r"^(.+)[@|©]", TextToAnalyze)
        user = m0.group(1).strip()
    except:
        user = ""

    print("DEBUG:"+user)
    try:
        m1 = re.search(r"([@|©].[A-Za-z0-9]*) ",TextToAnalyze)
        handle = m1.group(1).strip()
        print(user)
    except:
        handle = ""

    print("DEBUG:"+handle)

    m2 = re.search(r"([0-9]+\sSep)", TextToAnalyze)
    date = m2.group(1).strip()


    m3 = re.search(r"Q ([1-9]+)", TextToAnalyze)
    comments = m3.group(1).strip()

    m4 = re.search(r"U ([1-9]+)", TextToAnalyze)
    retweets = m4.group(1).strip()

    m5 = re.search(r"O ([1-9]+)", TextToAnalyze)
    likes = m5.group(1).strip()

    m6 = re.search(r"[0-9]{1,2} Sep(.*)Q [0-9]*", TextToAnalyze,re.MULTILINE | re.DOTALL)
    body = m6.group(1).strip()

    pol,subj = GetSentiment(body)


    return jsonify({"user" : user,"handle":handle,"date":date,"comments":comments,"retweets":retweets,"likes":likes,"body":body,"polarity":pol,"subjectivity":subj})
