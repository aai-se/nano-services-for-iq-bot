#######################################
#
# Nano Service to convert Country Name to Country Code (ex: France to FR)
#
DESCRIPTION = "Apply Transformation to Image"
VERSION = 0.1
## Prerequisites:
# Install Python 3.7+ (make sure to add python to your PATH, the installer has an option for it)
# Install Opencv for python: pip install opencv-python
#######################################
import os
import cv2 # Imports Opencv module
from flask import Flask,request,jsonify

SupportedTransformations = ["morph_ex","regular_binarization","otsu_binarization","clahe_histo","regular_histo","gray_scaling","interpolation","unsharp","blur"]
InputFilePathParameter = "in_filepath"
OutputFilePathParameter = "out_filepath"
MethodParameter = "method"

Helper = {
    "_desc":DESCRIPTION,
    "_version":VERSION,
    "Mandatory Input Parameters" : [InputFilePathParameter,OutputFilePathParameter,MethodParameter],
    "Optional Input Parameters" : ""
}

app = Flask(__name__)

@app.route("/")
def hello():
    return jsonify(Helper)

#Expected:
# {"filepath":"c:/test/my.png"}
@app.route("/transform", methods=['POST'])
def transformFile():
    content = request.get_json()
    if InputFilePathParameter not in content:
        return jsonify({"Status" : "Error","Message":"Missing "+InputFilePathParameter+" parameter"})

    if OutputFilePathParameter not in content:
        return jsonify({"Status" : "Error","Message":"Missing "+OutputFilePathParameter+" parameter"})

    if MethodParameter not in content:
        return jsonify({"Status" : "Error","Message":"Missing "+MethodParameter+" parameter"})

    InputFilePath = content['in_filepath']
    OutFilePath = content['out_filepath']
    Method = content['method']

    #print("File:"+FilePath)
    image = cv2.imread(InputFilePath,0)

    #cv2.imshow('Before',image) # Shows the image graphically
    #cv2.waitKey(0) # Waits for the next key to be pressed
    myNewImg = OutFilePath

    if Method.lower() == "otsu_binarization":
        #myNewImg = getOutputFilePath(InputFilePath,"BIN")
        output = OTSUBinarization(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    elif Method.lower() == "regular_binarization":
        #myNewImg = getOutputFilePath(InputFilePath,"BIN")
        output = RegularBinarization(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    elif Method.lower() == "clahe_histo":
        #myNewImg = getOutputFilePath(InputFilePath,"INT")
        output = ClaheHistoEqualization(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    elif Method.lower() == "regular_histo":
        #myNewImg = getOutputFilePath(InputFilePath,"INT")
        output = HistoEqualization(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    elif Method.lower() == "gray_scaling":
        #myNewImg = getOutputFilePath(InputFilePath,"INT")
        output = GrayScaling(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    elif Method.lower() == "interpolation":
        #myNewImg = getOutputFilePath(InputFilePath,"INT")
        output = IntercubicInterpolation(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    elif Method.lower() == "unsharp":
        #myNewImg = getOutputFilePath(InputFilePath,"UNS")
        output = UnsharpMasking(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    elif Method.lower() == "blur":
        #myNewImg = getOutputFilePath(InputFilePath,"UNS")

        output = Blur(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    elif Method.lower() == "morph_ex":
        #myNewImg = getOutputFilePath(InputFilePath,"UNS")
        output = MorphEx(image)
        if output.error == None:
            cv2.imwrite(myNewImg,output.image)
            return jsonify({"Status" : "Success","Message":""})
        else:
            return jsonify({"Status" : "Error","Message":str(output.error)})

    else:
        return jsonify({"Status" : "Error","Message":"Invalid Method"})


    # Display the new Binarized File side by side with the original file
    #cv2.imshow('After',thresh1) # Shows the image graphically
    #cv2.waitKey(0) # Waits for the next key to be pressed
    #cv2.destroyAllWindows() # Closes the window

################################
###### OpenCV Methods ##########
################################

#blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, rectKernel)
def MorphEx(image):
    #Inter-Cubic Interpolation
    try:
        rectKernel = cv2.StructuralElement(cv2.MORPH_RECT, (154,682),(965,683))
        processedImage = cv2.morphologyEx(image, cv2.MORPH_BLACKHAT, rectKernel)
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut



def Blur(image):
    #Inter-Cubic Interpolation
    try:
        processedImage = cv2.blur(image,(3,3))
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut


def ClaheHistoEqualization(image):
    #Inter-Cubic Interpolation
    try:
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        processedImage = clahe.apply(image)
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut

def HistoEqualization(image):
    #Inter-Cubic Interpolation
    try:
        processedImage = cv2.equalizeHist(image)
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut

def GrayScaling(image):
    #Inter-Cubic Interpolation
    try:
        processedImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut

def UnsharpMasking(image):
    #Inter-Cubic Interpolation
    try:
        gaussian_3 = cv2.GaussianBlur(image, (1,1), 0)
        processedImage = cv2.addWeighted(image, 1.5, gaussian_3, -0.5, 0, image)
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut

def IntercubicInterpolation(image):
    #Inter-Cubic Interpolation
    try:
        processedImage = cv2.resize(image, None, fx=3, fy=3, interpolation=cv2.INTER_CUBIC)
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut

def OTSUBinarization(image):
    #Inter-Cubic Interpolation
    try:
        ret,processedImage = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut

def RegularBinarization(image):
    #Inter-Cubic Interpolation
    try:
        ret,processedImage = cv2.threshold(image,127,255,cv2.THRESH_BINARY)
        ObjOut = CVOutput(processedImage,None)
    except Exception as e:
        ObjOut = CVOutput(None,e)
    return ObjOut

#################################
####### OTHER FUNCTIONS #########
#################################

def getOutputFilePath(myPath,prefix):
    mySplit = os.path.split(myPath)
    PathOnly=mySplit[0]
    FileNameWithExt=mySplit[1]
    FileName=FileNameWithExt.split(".")[0]
    Ext=FileNameWithExt.split(".")[1]
    OutputPath=PathOnly+'/'+prefix+"_"+FileNameWithExt
    return OutputPath

class CVOutput:
    def __init__(self,image,error):
        self.image = image
        self.error = error
