#######################################
#
# Nano Service String matching Operations
#
# Prerequisites:
# Install Python 3.7+ (make sure to add python to your PATH, the installer has an option for it)
#   Install fuzzywuzzy
#
########################################
DESCRIPTION = "String Matching Service"
VERSION = 0.1
#
#######################################
import os,json
from fuzzywuzzy import fuzz,process
from flask import Flask,request,jsonify

def LoadConfigFile():
    BaseName = os.path.basename(__file__)
    FileNameNoExt = os.path.split(__file__)[1].split(".")[0]
    ConfigFile = "./services/"+FileNameNoExt+".json"
    with open(ConfigFile) as json_data_file:
        data=json_data_file.read()
        #confdata = json.load(json_data_file)
    JSON_CONF = json.loads(data)
    return JSON_CONF


app = Flask(__name__)
app.secret_key = 'Oh what a big secret this is!!'

SupportedOperations = ["ratio","partial_ratio","token_sort_ratio","token_set_ratio"]


# Done, returns the list of folders under the IQ Bot output folder (List of Learning Instances)
@app.route('/compare', methods = ['GET', 'POST'])
def GetMatchingRatio ():
    content = request.get_json()
    if 'method' not in content:
        return jsonify({"Status" : "Error","Message":"Missing method parameter"})
    if 'string1' not in content:
        return jsonify({"Status" : "Error","Message":"Missing string1 parameter"})
    if 'string2' not in content:
        return jsonify({"Status" : "Error","Message":"Missing string2 parameter"})

    Method = content['method']
    Str1 = content['string1']
    Str2 = content['string2']

    # Levenshtein distance
    if Method.lower() == "ratio":
        Ratio = fuzz.ratio(Str1.lower(),Str2.lower())

    # if the short string has length k and the longer string has the length m, then the algorithm seeks the score of the best matching length-k substring.
    elif Method.lower() == "partial_ratio":
        Ratio = fuzz.partial_ratio(Str1.lower(),Str2.lower())

    # Strings are tokenized, lower cased, punctuation is removed and alphabetically sorted, THEN compared
    elif Method.lower() == "token_sort_ratio":
        Ratio = fuzz.token_sort_ratio(Str1,Str2)

    # Takes out comon tokens (after tokenization)
    elif Method.lower() == "token_set_ratio":
        Ratio = fuzz.token_set_ratio(Str1,Str2)

    else:
        return jsonify({"Status" : "Error","Message":"method not supported:"+Method})

    return jsonify({"ratio" : Ratio})

# Done, returns the list of folders under the IQ Bot output folder (List of Learning Instances)
@app.route('/closestmatch', methods = ['GET', 'POST'])
def GetClosestMatch ():
    content = request.get_json()
    if 'string' not in content:
        return jsonify({"Status" : "Error","Message":"Missing string parameter"})
    if 'options' not in content:
        return jsonify({"Status" : "Error","Message":"Missing options parameter"})

    Str1 = content['string']
    Options = content['options']

    Ratios = process.extract(Str1,Options)
    #print(Ratios)

    highest = process.extractOne(Str1,Options)
    #print(highest)

    return jsonify({"all":Ratios,"highest" : highest})
