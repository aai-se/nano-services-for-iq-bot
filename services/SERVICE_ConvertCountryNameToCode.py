#######################################
#
# Nano Service to convert Country Name to Country Code (ex: France to FR)
#
DESCRIPTION = "Converts Country Name to 2 Char Code"
VERSION = 0.1
#
#######################################

import pycountry
from flask import Flask,request,jsonify
import difflib

app = Flask(__name__)


countriesNameTo2Char = {}
for country in pycountry.countries:
    countriesNameTo2Char[country.name] = country.alpha_2

countriesNameTo3Char = {}
for country in pycountry.countries:
    countriesNameTo3Char[country.name] = country.alpha_3


countries3Charto2Char = {}
for country in pycountry.countries:
    countries3Charto2Char[country.alpha_3] = country.alpha_2


@app.route("/")
def hello():
    return '{"desc":'+DESCRIPTION+',"version":"'+VERSION+'"}'

#Expected:
# {"country":"China"}
#Returns: CN
@app.route("/get2charcode", methods=['POST'])
def getCode():
    #print (request.is_json)
    content = request.get_json()
    countryName = content['country']
    CountryNameClosestMatch = getClosestMatch(countryName)
    #print("DEBUG:"+content['country'])
    Code = get2CharCodeFromCountryName(CountryNameClosestMatch)
    #print (Code)
    #return jsonify({"code":Code})
    return Code

#Expected:
# {"country":"China"}
#Returns: CHN
@app.route("/get3charcode", methods=['POST'])
def getCode2():
    #print (request.is_json)
    content = request.get_json()
    countryName = content['country']
    CountryNameClosestMatch = getClosestMatch(countryName)
    #print("DEBUG:"+content['country'])
    Code = get3CharCodeFromCountryName(CountryNameClosestMatch)
    #print (Code)
    #return jsonify({"code":Code})
    return Code

#Expected:
# {"code":"CHN"}
#Returns: CN
@app.route("/convert3CharTo2Char", methods=['POST'])
def getCode3():
    #print (request.is_json)
    content = request.get_json()
    countryCode = content['code']

    #print("DEBUG:"+countryCode)
    Code = countries3Charto2Char[countryCode]
    #print (Code)
    #return jsonify({"code":Code})
    return Code


def get2CharCodeFromCountryName(country):
    return countriesNameTo2Char.get(country, 'Unknown')

def get3CharCodeFromCountryName(country):
    return countriesNameTo3Char.get(country, 'Unknown')


def getClosestMatch(country):
    Resp = difflib.get_close_matches(country, countriesNameTo2Char)
    return Resp[0]
