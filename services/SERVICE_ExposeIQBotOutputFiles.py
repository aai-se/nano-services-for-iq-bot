#######################################
#
# Nano Service to expose IQ Bot output files to download
#
DESCRIPTION = "Exposes IQ Bot output Files"
VERSION = 0.1
#
#######################################
import os,json
from flask import Flask,request,jsonify,flash,redirect,url_for,send_file
from werkzeug.utils import secure_filename
from datetime import datetime

def LoadConfigFile():
    BaseName = os.path.basename(__file__)
    FileNameNoExt = os.path.split(__file__)[1].split(".")[0]
    ConfigFile = "./services/"+FileNameNoExt+".json"
    with open(ConfigFile) as json_data_file:
        data=json_data_file.read()
        #confdata = json.load(json_data_file)
    JSON_CONF = json.loads(data)
    return JSON_CONF


CONFIG_DATA = {}

CONFIG_DATA = LoadConfigFile()
UPLOAD_FOLDER = CONFIG_DATA['UPLOAD_FOLDER']
OUTPUT_PATH = CONFIG_DATA['OUTPUT_PATH']
ALLOWED_EXTENSIONS = CONFIG_DATA['ALLOWED_EXTENSIONS']
SUPPORTED_TYPES = CONFIG_DATA['SUPPORTED_TYPES']

app = Flask(__name__)
app.secret_key = 'Oh what a big secret this is!!'
app.config['SESSION_TYPE'] = 'filesystem'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_PATH'] = OUTPUT_PATH

# Done, returns the list of folders under the IQ Bot output folder (List of Learning Instances)
@app.route("/list")
def ListFiles ():
    try:
        AllFolders = getAllowedPaths()
        RawJsonArray = json.dumps(AllFolders)
        JSON = {
        "folders":AllFolders
        }
        return jsonify(JSON)
    except Exception as e:
        return "Error:" +str(e)

# Returns the list of Files under a Type (Invalid, Success or Unclassified) for any given Learning Instance
@app.route("/list/<LearningInstanceName>/<Type>")
def ListFilesFromLIs (LearningInstanceName,Type):
    try:
        ListOfFiles = getListOfFiles(LearningInstanceName,Type)
        JSON = {
        "files":ListOfFiles
        }
        return jsonify(JSON)
    except Exception as e:
        return "Error:" +str(e)

# Downlads a named file for a LI, in a folder of a Type (Invalid, Success or Unclassified)
@app.route("/download/<LearningInstanceName>/<Type>/<filename>")
def DownloadFile (LearningInstanceName,Type,filename):

    try:
        MyPath = OUTPUT_PATH+"/"+LearningInstanceName+"/"+Type+"/"+filename
        #print("DEBUG:"+MyPath)
        return send_file(MyPath, as_attachment=True)
    except Exception as e:
        return "Error: " + str(e)

# Uploads a file into the UPLOAD FOLDER on the Server Side
@app.route('/upload/<LearningInstanceName>', methods = ['GET', 'POST'])
def upload_file(LearningInstanceName):
    if LearningInstanceName == "":

        return jsonify({"Status" : "Error","Message":"No Learning Instance Name passed."})
    if request.method == 'POST':
        f = request.files['files']
        NewFileName = os.path.join(app.config['UPLOAD_FOLDER']+"/"+LearningInstanceName, secure_filename(f.filename))
        #print("Debug:"+NewFileName)
        try:
            f.save(NewFileName)
            return jsonify({"Status":"OK","Message":""})
        except Exception as e:
            # Do we want to create the folder if it doesnt exist?
            return jsonify({"Status":"Error","Message":str(e)})
    else:
        return jsonify({"Status":"Error","Message":""})
    return jsonify({"Status" : "Error","Message":"Wrong Method"})


def getAllowedPaths():
    #remove the backupdate folder
    AllFolders = os.listdir(OUTPUT_PATH)
    AllFolders.remove("BackupData")
    return AllFolders

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def getListOfFiles(FolderName,Type):
    if Type in SUPPORTED_TYPES:
        LocalDirToSearch = OUTPUT_PATH+"/"+FolderName+"/"+Type
        RES = os.walk(LocalDirToSearch)
        Res_Files = [x[2] for x in RES]

        AllFiles = []
        for f in Res_Files[0]:

            ctime = os.path.getctime(LocalDirToSearch+"/"+f)
            mtime = os.path.getmtime(LocalDirToSearch+"/"+f)
            ReadableCreationTime = datetime.fromtimestamp(ctime).strftime('%Y-%m-%d %H:%M:%S')
            ReadableModTime = datetime.fromtimestamp(mtime).strftime('%Y-%m-%d %H:%M:%S')
            ReadableSize = os.path.getsize(LocalDirToSearch+"/"+f)

            myItem = FileItem(f,ReadableCreationTime,ReadableModTime,ReadableSize)
            AllFiles.append(myItem)

        if Res_Files:
            #print("Results for ["+str(dir)+"]:")
            return [ob.__dict__ for ob in AllFiles]

        else:
            return []
    else:
        return []

class FileItem:
    def __init__(self,name,cdate,mdate,size):
        self.name = name
        self.cdate = cdate
        self.mdate = mdate
        self.size = size
