# Nano Services for IQ Bot and all RPA Solutions

small Flask based Rest Services for IQ Bot and RPA Solutions

* Convert Country Name to Country Code (2 letter code. Ex: France -> FR)
* Reverse the pages in a local pdf file
* Apply OpenCV Transformations to file
* Expose IQ Bot output files 
* NLP (NER, Sentiment Analysis and Language Detection)
* String Matching (Levenshtein distance, etc.)